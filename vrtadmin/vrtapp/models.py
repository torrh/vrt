# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from datetime import datetime

# Create your models here.
class Summary(models.Model):
    date_test = models.DateTimeField(default=datetime.now, blank=True)
    snapshot_before = models.TextField(max_length=1000)
    snapshot_after = models.TextField(max_length=1000)
    snapshot_result = models.TextField(max_length=1000, default="")
    comments = models.TextField(max_length=1000, null=True)