describe('Random color test', function() {

    it('Execute random color test two times', function() {


        cy.on('uncaught:exception', (err, runnable) => {
            expect(err.message).to.include('something about the error')
            // using mocha's async done callback to finish
            // this test so we prove that an uncaught exception
            // was thrown
            done()

            // return false to prevent the error from
            // failing this test
            return false
        })

        cy.visit('https://torrh.github.io/VRT_colorPallete/')

        cy.get('#random-button').click()

        cy.screenshot('snapshot_before')

        cy.get('#random-button').click()

        cy.screenshot('snapshot_after')
    })
})
