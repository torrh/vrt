# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.urls import reverse
from django.shortcuts import render, redirect
from .models import Summary
# Create your views here.
import random
import shutil

import subprocess

def index(request):

    if request.method == 'POST':


        x = 11
        snapshot_id = long(random.random() * 100000000000000000L)

        path_snapshot_before = './snapshots/before_{0}.png'.format(snapshot_id)
        path_snapshot_after = './snapshots/after_{0}.png'.format(snapshot_id)
        path_snapshot_result = './snapshots/result_{0}.png'.format(snapshot_id)

            #process = subprocess.Popen(["./node_modules/cypress/bin/cypress", "run"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            #process.communicate()

        file_base = './vrtresemble/base_images/screenshot_base_{0}.png'.format(x)
        file_mutant = './vrtresemble/mutant_images/screenshot_{0}.png'.format(x)

        shutil.copyfile(file_base, path_snapshot_before)
        shutil.copyfile(file_mutant, path_snapshot_after)

        process = subprocess.Popen(["node", "./vrtresemble/vrtresemble.js"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, error = process.communicate()

        file_result = './vrtresemble/results/result_{0}.png'.format(x)

        shutil.copyfile(file_result, path_snapshot_result)

        print ".........................................."
        print path_snapshot_before[1:]
        print path_snapshot_after[1:]
        print path_snapshot_result[1:]
        print ".........................................."

        Summary.objects.all().delete()

        summary = Summary()
        summary.snapshot_before = path_snapshot_before[1:]
        summary.snapshot_after = path_snapshot_after[1:]
        summary.snapshot_result = path_snapshot_result[1:]
        summary.comments = out

        summary.save()

        return redirect(reverse('vrt:index'))
    else:
        summary_list = Summary.objects.all()
        context = {'list_summary': summary_list}
        return render(request, 'vrtapp/index.html', context)