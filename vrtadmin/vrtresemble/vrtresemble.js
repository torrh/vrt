const compareImages = require('resemblejs/compareImages');
const fs = require("mz/fs");

async function getDiff(){
    const options = {

         output: {
            errorColor: {
                red: 255,
                green: 0,
                blue: 255
            },
            errorType: "movement",
            transparency: 0.3,
            largeImageThreshold: 1200,
            useCrossOrigin: false,
            outputDiff: true
        },
        scaleToSameSize: true,
        ignore: ['less'],
    };

    var x = 11

    var result = "./vrtresemble/results/result_"+x+".png"
    var base = "./vrtresemble/base_images/screenshot_base_"+x+".png"
    var mutant = "./vrtresemble/mutant_images/screenshot_"+x+".png"

    const data = await compareImages(
        await fs.readFile(base),
        await fs.readFile(mutant),
        options
    );

    await fs.writeFile(result, data.getBuffer());
}

getDiff();

